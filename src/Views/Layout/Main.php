<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Code news</title>
    <link rel="stylesheet" href="./css/common.css" type="text/css">
    <link rel="stylesheet" href="./css/header.css" type="text/css">
    <link rel="stylesheet" href="./css/footer.css" type="text/css">
</head>

<body>
    <?php require_once(__DIR__ . '/Header.php'); ?>
    <main>
        <?= $content ?>
    </main>
    <?php require_once(__DIR__ . '/Footer.php'); ?>
</body>