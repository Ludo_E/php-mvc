<?php
// définition du titre de la page
$title = "Liste des articles";
?>

<h2>Articles</h2>
<?php
// récupération des utilisateurs en utilisant le DAO
foreach ($articles as $article) {
?>
	<div class="article">
    	<h3>
        	<?= htmlspecialchars($article->getTitre()); ?>
        	<em>le <?= $post['french_creation_date']; ?></em>
    	</h3>
    	<p>
        	<?= nl2br(htmlspecialchars($article->getTexte())); ?>
    	</p>
	</div>
	
<?php
}
?>