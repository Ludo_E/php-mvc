<?php

namespace Afpa\Models;

use Afpa\Tools\Database;

use PDO;

/**
 * Classe à utiliser pour requêter la base de données pour récupérer les informations des utilisateurs.
 */
class UtilisateurDAO {

    /**
     * Permet d'obtenir un tableau de tous les utilisateurs.
     */
    public static function getAll(): array {
        // récupération de l'objet de requêtage
        $pdo = Database::connect();

        // instanciation d'un tableau de stagaires
        $utilisateurs = array();
        $sql = "SELECT * from utilisateur";

        $results = $pdo->query($sql);

        // boucle sur toutes les lignes de résultat
        foreach ($results as $row) {
            // récupération des valeurs des enregistrements de la base de données
            $id = $row['id'];
            $prenom = $row['prenom'];
            $nom = $row['nom'];
            $email = $row['email'];
            $password = $row['password'];

            // instanciation d'un nouvel utilisateur
            $utilisateur = new Utilisateur($id, $prenom, $nom, $email, $password);
            
            array_push($utilisateurs, $utilisateur);
        }

        // déconnexion de la base de données
        Database::disconnect();
        return $utilisateurs;
    }

    /**
     * Permet d'obtenir un utilisateur par son identifiant
     */
    public static function getById(int $id): Utilisateur {
        // récupération de l'objet de requêtage
        $pdo = Database::connect();
        $statement = $pdo->prepare("SELECT * from utilisateur WHERE :id");

        // association des paramètres sur la requête
        $statement->bindParam("id", PDO::PARAM_INT);
        $statement->execute();

        // récupération du résultat
        $row_result = $statement->fetch();

        // récupération des résultats
        $id = $row_result['id'];
        $prenom = $row_result['prenom'];
        $nom = $row_result['nom'];
        $email = $row_result['email'];
        $password = $row_result['password'];
        // instanciation de l'utilisateur
        $utilisateur = new Utilisateur($id, $prenom, $nom, $email, $password);

        return $utilisateur;
    }
}