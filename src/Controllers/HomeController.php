<?php

namespace Afpa\Controllers;

use Afpa\Core\Controller;

class HomeController extends Controller 
{
    public function index()
    {
        $name = "Ada";
        $data = compact("name");

        $this->render('Home', $data);
    }
}