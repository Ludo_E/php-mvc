<?php

namespace Afpa;

use Afpa\Core\Router;

use Afpa\Controllers\HomeController;
use Afpa\Controllers\ArticleController;

// Instanciation du router
$router = new Router();

// Définiton de toutes les routes du site
$router->addRoute('/', HomeController::class, 'index');
$router->addRoute('/articles/list', ArticleController::class, 'index');
    